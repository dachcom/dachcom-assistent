<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.dachcom.com
 * @since      1.0.0
 *
 * @package    Dachcom_Assistent
 * @subpackage Dachcom_Assistent/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Dachcom_Assistent
 * @subpackage Dachcom_Assistent/includes
 * @author     Stefan Hagspiel <shagspiel@dachcom.ch>
 */
class Dachcom_Assistent_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		if( !class_exists('DachcomPluginObserver') ) {

			wp_die('Plugins, welche vom <a href="http://dachcom-digital.ch">Dachcom Digital Team</a> entwickelt wurden, benötigen das "Dachcom Plugin Observer" MU-Plugin. <a href="'. admin_url( 'plugins.php' ) . '">Zur&uuml;ck</a>.');
			return;

		}

	}

}
