<?php

class Dachcom_Assistent_Manager {


    private static $_instance = false;

    /**
     * @var Assistents
     */
    private $_assistents;

    protected function __construct() {

        self::addDefaultAssistents();

    }

    public static function factory($options = array())
    {
        if (self::$_instance === false) {
            self::$_instance = new Dachcom_Assistent_Manager( $options );
        }
        return self::$_instance;
    }

    /**
     * Add default assistents
     *
     * @return void
     */
    protected function addDefaultAssistents()
    {
        $this->add('mail', new Dachcom_Assistent_Mail());
    }

    /**
     * Get a assistent by name.
     *
     * @param string $name assistent name
     *
     * @return callable Dachcom_Assistent_Core
     */
    public function getAssistent($name)
    {
        return $this->__get($name);
    }

    /**
     * Check whether this instance has a assistent.
     *
     * @param string $name assistent name
     *
     * @return boolean True if the assistent is present
     */
    public function hasAssistent($name)
    {
        return $this->has($name);
    }

    /**
     * Add a new assistent to assistents
     *
     * @param string $name   assistent name
     * @param mixed  $assistent
     *
     * @throws \InvalidArgumentException if $assistent is not a callable
     * @return void
     */
    public function add($name, $assistent)
    {
        if (!is_callable($assistent) && !$assistent instanceof Dachcom_Assistent_Core) {
            throw new \InvalidArgumentException(
                "$name Assistent is not a callable or doesn't extend the Dachcom_Assistent_Core."
            );
        }
        $this->_assistents[$name] = $assistent;
    }

    /**
     * Calls a assistent, whether it be a Closure or Dachcom_Assistent_Core instance
     *
     * @param string               $name     The name of the assistent
     *
     * @throws \InvalidArgumentException
     * @return mixed The assistent return value
     */
    public function call( $name )
    {

        $name = str_replace('Assistent', '', $name );

        if (!$this->has($name)) {

            throw new InvalidArgumentException('Unknown assistent: ' . $name);
        }

        if ($this->_assistents[$name] instanceof Dachcom_Assistent_Core) {
            return $this->_assistents[$name];
        }

        return false;
    }


    /**
     * Check if $name assistent is available
     *
     * @param string $name assistent name
     *
     * @return boolean
     */
    public function has($name)
    {
        return array_key_exists($name, $this->_assistents);
    }

    /**
     * Get a assistent.
     *
     * @param string $name assistent name
     *
     * @throws \InvalidArgumentException if $name is not available
     * @return callable assistent function
     */
    public function __get($name)
    {
        if (!$this->has($name)) {
            throw new \InvalidArgumentException('Unknown assistent :' . $name);
        }
        return $this->_assistents[$name];
    }

    /**
     * Clear the assistent collection.
     *
     * Removes all assistents from this collection
     *
     * @return void
     */
    public function clear()
    {
        $this->_assistents = array();
    }
    /**
     * Check whether the assistent collection is empty.
     *
     * @return boolean True if the collection is empty
     */
    public function isEmpty()
    {
        return empty($this->_assistents);
    }

    /**
     * Returns all assistents from the collection.
     *
     * @return array Associative array of assistents which keys are assistents names
     * and the values are the assistents.
     */
    public function getAll()
    {
        return $this->_assistents;
    }
}