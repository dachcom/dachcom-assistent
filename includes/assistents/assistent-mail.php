<?php

class Dachcom_Assistent_Mail extends Dachcom_Assistent_Core {

    var $header = array();

    /**
     * @param array $params
     * @return bool
     */
    public function send_mail( $args ) {

        $default = array(
            'recipient' => NULL,
            'subject' => '',
            'message' => '',
            'copy' => FALSE,
            'attachments' => array(),
            'html' => FALSE,
            'header' => array()
        );

        $args = wp_parse_args( $args, $default );

        $recipient = NULL;

        if( !empty( $args['recipient'] ) ) {

            if(is_array( $args['recipient'] ))
                $recipient 	= implode(', ', $args['recipient']);
            else
                $recipient 	= $args['recipient'];
        }

        if( is_null($recipient) )
            $recipient =  implode(', ', (array) $this->get_field('default_email_recipient', 'option') );

        $headers_default = array();
        $headers_default['from'] = 'From: ' . $this->get_field('default_email_sender_name', 'option') . ' <' . $this->get_field('default_email_sender_mail', 'option') . '>';

        $header = array_merge($headers_default, $args['header']);

        if( $args['html'] ) {

            $header[] = 'MIME-Version: 1.0';
            $header[] = 'Content-type: text/html; charset=UTF-8';

        }

        wp_mail($recipient, $args['subject'], $args['message'], $header, $args['attachments']);

        if($args['copy'] !== FALSE ) {

            wp_mail($args['copy'], 'Kopie: ' . $args['subject'], $args['message'], $header, $args['attachments']);

        }

        //now we do not need the attachments any more: delete them...
        if(!empty($files)) {

            foreach($files as $file)
                @unlink($file);

        }

        return true;

    }

    private function get_field( $field_name = '', $post_id = 0, $format_value = TRUE ) {

        //if acf is installed.
        if ( function_exists( 'get_field' ) ) {

            return get_field( $field_name, $post_id , $format_value );

        }

        return 'value missing for "' . $field_name . '"';

    }

}