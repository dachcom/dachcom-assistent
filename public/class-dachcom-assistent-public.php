<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.dachcom.com
 * @since      1.0.0
 *
 * @package    Dachcom_Assistent
 * @subpackage Dachcom_Assistent/public
 */

/**
 *
 * @package    Dachcom_Assistent
 * @subpackage Dachcom_Assistent/public
 * @author     Stefan Hagspiel <shagspiel@dachcom.ch>
 */
class Dachcom_Assistent_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $dachcom_assistent       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}



}
