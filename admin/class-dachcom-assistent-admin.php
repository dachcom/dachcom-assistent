<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.dachcom.com
 * @since      1.0.0
 *
 * @package    Dachcom_Assistent
 * @subpackage Dachcom_Assistent/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Dachcom_Assistent
 * @subpackage Dachcom_Assistent/admin
 * @author     Stefan Hagspiel <shagspiel@dachcom.ch>
 */
class Dachcom_Assistent_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * This ist required to register Plugin in Dachcom Repo
	 *
	 * @var DachcomPluginObserver_Connector
	 */
	private $dpo_connector;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	public function register_plugin() {

		$this->dpo_connector = new DachcomPluginObserver_Connector();
		$this->dpo_connector->connect_to_plugin_checker( $this->plugin_name, plugin_dir_path( dirname( __FILE__ ) ) . $this->plugin_name . '.php' );

	}

}
